/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package in.ac.gpckasaragod.id.proof.collection.system.ui;

import in.ac.gpckasaragod.id.proof.collection.system.model.IdProofDetail;
import in.ac.gpckasaragod.id.proof.collection.system.service.IdProofDetailsService;
import in.ac.gpckasaragod.id.proof.collection.system.service.impl.IdProofDetailsServiceImpl;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author student
 */
public class IdProofDetailsForm extends javax.swing.JFrame {

    IdProofDetailsService idProofDetailsService = new IdProofDetailsServiceImpl();

    /**
     * Creates new form IdProofDetails1
     */
    public IdProofDetailsForm() {
        initComponents();
        setLocationRelativeTo(null);
        configureTable();
        loadTableValues();
    }

    public IdProofDetailsForm(String idProofType, String number) {

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     */
    private void configureTable() {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("id");
        model.addColumn("idProofType");
        model.addColumn("number");
        tblIdProofDetails.setModel(model);

    }

    private void loadTableValues() {
        List<IdProofDetail> idProofDetails = idProofDetailsService.getAllIdProofDetails();

        DefaultTableModel model = (DefaultTableModel) tblIdProofDetails.getModel();
        model.setRowCount(0);
        for (IdProofDetail idProofDetail : idProofDetails) {
            model.addRow(new Object[]{
                idProofDetail.getId(), idProofDetail.getIdProofType(), idProofDetail.getNumber()
            });
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblIdProofDetails = new javax.swing.JTable();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tblIdProofDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "ID PROOF TYPE", "NUMBER"
            }
        ));
        jScrollPane1.setViewportView(tblIdProofDetails);

        btnAdd.setText("ADD");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setText("EDIT");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setText("DELETE");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 558, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(93, 93, 93)
                        .addComponent(btnAdd)
                        .addGap(78, 78, 78)
                        .addComponent(btnEdit)
                        .addGap(60, 60, 60)
                        .addComponent(btnDelete)))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnEdit)
                    .addComponent(btnDelete))
                .addGap(27, 27, 27))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        // TODO add your handling code here:
        IdProofDetailForm idProofDetail = new IdProofDetailForm();
        JFrame window = new JFrame();
        window.add(idProofDetail);
        window.pack();
        window.setVisible(true);
        window.setLocationRelativeTo(null);
        window.addWindowListener(new WindowAdapter() {

            public void WindowClosing(WindowEvent e) {
                loadTableValues();
                System.out.print("window closed");
            }

            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                System.out.print("window closing");
                loadTableValues();
            }
        });
    }//GEN-LAST:event_btnAddActionPerformed


    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        // TODO add your handling code here:
        Integer rowIndex = tblIdProofDetails.convertRowIndexToModel(tblIdProofDetails.getSelectedRow());
        Integer id = (Integer) tblIdProofDetails.getValueAt(rowIndex, 0);
        IdProofDetail idProofDetail = idProofDetailsService.readIdproofDetails(id);
        JFrame Window = new JFrame();
        Window.setVisible(true);
        IdProofDetailForm idProofDetailForm = new IdProofDetailForm(idProofDetail);
        Window.add(idProofDetailForm);
        Window.setSize(600, 600);
        Window.pack();
        Window.setLocationRelativeTo(null);
        Window.addWindowListener(new WindowAdapter() {
            public void Windowclosed(WindowEvent e) {
                loadTableValues();
                System.out.println("Window Closed");
            }

            public void WindowClosing(WindowEvent e) {
                super.windowClosing(e);
                System.out.println("Window Closing");
                loadTableValues();
            }
        });
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        int rowIndex = tblIdProofDetails.convertRowIndexToModel(tblIdProofDetails.getSelectedRow());
        Integer id = (Integer) tblIdProofDetails.getValueAt(rowIndex, 0);
        int result = JOptionPane.showConfirmDialog(IdProofDetailsForm.this, "Are you sure to delete","Delete",JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            String message = idProofDetailsService.deleteIdProofDetails(id);
            JOptionPane.showMessageDialog(IdProofDetailsForm.this, message);
            loadTableValues();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(IdProofDetailsForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(IdProofDetailsForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(IdProofDetailsForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IdProofDetailsForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and disp.lay the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new IdProofDetailsForm().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblIdProofDetails;
    // End of variables declaration//GEN-END:variables
}
