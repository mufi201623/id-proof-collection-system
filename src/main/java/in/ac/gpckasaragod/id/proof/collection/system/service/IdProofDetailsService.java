/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.id.proof.collection.system.service;

import in.ac.gpckasaragod.id.proof.collection.system.model.IdProofDetail;

import java.util.List;

/**
 *
 * @author student
 */
public interface IdProofDetailsService {
   public String saveIdproofDetails(String idprooftype,String number);
   public IdProofDetail readIdproofDetails(Integer id);
   public List<IdProofDetail> getAllIdProofDetails();
   public String deleteIdProofDetails(Integer id);
   public String updateIdproofDetails(Integer id,String idProofType,String number);
}
