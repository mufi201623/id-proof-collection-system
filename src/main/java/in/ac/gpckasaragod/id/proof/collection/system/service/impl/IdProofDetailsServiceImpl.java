/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.id.proof.collection.system.service.impl;


import in.ac.gpckasaragod.id.proof.collection.system.model.IdProofDetail;
import in.ac.gpckasaragod.id.proof.collection.system.service.IdProofDetailsService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class IdProofDetailsServiceImpl extends ConnectionServiceImpl implements IdProofDetailsService {
    @Override
    public String saveIdproofDetails(String idProofType, String number) {
        // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = " INSERT INTO ID_PROOF_DETAIL(IdProofType,number)Values "
                    + "('" + idProofType + "','" + number + "')";
            System.err.println("Query:" + query);

            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";
            } else {
                return "Saved successfully";
            }

        } catch (SQLException ex) {
            Logger.getLogger(IdProofDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "Save failed";

    }

    @Override
    public IdProofDetail readIdproofDetails(Integer id) {
        // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        IdProofDetail idProofDetail = null;

        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT *FROM ID_PROOF_DETAIL WHERE ID= " + id;
            ResultSet resultset = statement.executeQuery(query);
            while (resultset.next()) {
                String idProofType = resultset.getString("IDPROOFTYPE");
                String number = resultset.getString("NUMBER");
                idProofDetail = new IdProofDetail(id, idProofType, number);
            }
        } catch (SQLException ex) {

            Logger.getLogger(IdProofDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
           
        }
        return idProofDetail;
    }
    @Override
    public String deleteIdProofDetails(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM ID_PROOF_DETAIL WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete != 1) {
                return "Delete failed";
            } else {
                return "Deleted successfully";
            }

            
        } catch (SQLException ex) {
            Logger.getLogger(IdProofDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Delete failed";
        }
   
    }

    @Override
    public String updateIdproofDetails(Integer id,String idProofType,String number) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();

            String query = "UPDATE ID_PROOF_DETAIL SET IDPROOFTYPE='" + idProofType + "',NUMBER='" + number + "' WHERE ID=" + id;
            System.out.println(query);
            int update = statement.executeUpdate(query);
            if (update != 1) {
                return "Updated failed";
            } else {
                return "Updated successfully";

            }
        } catch (SQLException ex) {
            Logger.getLogger(IdProofDetailsServiceImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
            return "Update failed";
        }
    }

    @Override
    public List<IdProofDetail> getAllIdProofDetails() {
        List<IdProofDetail> idProofDetails=new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement=connection.createStatement();
            String query="SELECT * FROM ID_PROOF_DETAIL";
            ResultSet resultSet=statement.executeQuery(query);
            
            while(resultSet.next()) {
                Integer id =resultSet.getInt("ID");
                String idProofType=resultSet.getString("IDPROOFTYPE");
                String number =resultSet.getString("NUMBER");
                IdProofDetail idProofDetail=new IdProofDetail(id,idProofType,number);
                idProofDetails.add(idProofDetail);
            }
        } catch (SQLException ex) {
            Logger.getLogger(IdProofDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return idProofDetails;
    }
}
